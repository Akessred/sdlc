package test.com.maxim.jms.listener;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.maxim.jms.listener.ConsumerListener;

public class ConsumerListenerTest {

	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttest5\",firstName:\"BobTest5\",lastName:\"SmithTest5\",address:\"123 Main Test5\",city:\"TulsaTest5\",state:\"OKTest5\",zip:\"71345Test5\",email:\"Bob@microsoft.Test5\",phoneNumber:\"Test5-123-Test5\"}";
		
	@Before
	public void setUp() throws Exception {
		System.out.println("Test - setUp: start");
		System.out.println("Test - setUp: instance of context from /spring/application-config.xml");
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		System.out.println("Test - setUp: context instantiated");
		listener = (ConsumerListener)context.getBean("consumerListener");
		System.out.println("Test - setUp: listener");
		message = createMock(TextMessage.class);
		System.out.println("Test - setUp: createMock message");
		System.out.println("Test - setUp: end");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Test - tearDown: start");
		
		
		((ConfigurableApplicationContext)context).close();
		System.out.println("Test - tearDown: end");
	}

	@Test
	public void testOnMessage() throws JMSException {
		System.out.println("Test - testOnMessage: start");
		/*
		// No EasyMock
		//ConsumerListeneConsumerListener listener = new ConsumerListener();
		listener.onMessage(message);
		System.out.println("Test - testOnMessage: message: " + message.getText());
		assertNull(message);
		*/
		
		// System.out.println("Test - testOnMessage: message: " + message.getText());
		// When the getText() method of the message mock is called, the mock should return json object
		System.out.println("Test - testOnMessage - json: " + json);
		expect(message.getText()).andReturn(json);
		//System.out.println("Test - testOnMessage - expect - message: " + message.getText());
		System.out.println("Test - testOnMessage - expect");
		replay(message);
		System.out.println("Test - testOnMessage - replay");
		listener.onMessage(message);
		System.out.println("Test - testOnMessage - listener");
		verify(message);
		System.out.println("Test - testOnMessage - verify");
		
		System.out.println("Test - testOnMessage: end");
	}

}
